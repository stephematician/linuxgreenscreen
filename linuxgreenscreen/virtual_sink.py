# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import cv2
import numpy as np

from linuxgreenscreen.conversion_code import MAP_CV2_V4L2, SAFE_COLOR_CODES
from linuxgreenscreen.conversion_code import find_cv2_shape
from linuxgreenscreen.conversion_code import find_cv2_colorConversionCode
from linuxgreenscreen.wakeable_periodic import WakeablePeriodic
from v4l2py.device import BufferType, BufferManager, Device, Format, Priority


class VirtualSink:
    """A 'virtual' video sink using a v4l2loopback device.

    Open, close, change settings, and output frames to a v4l2loopback device.

    When open, a VirtualSink is a callable object for writing a buffered frame
    to the device. The buffer can be updated via :meth:`update_buffer`
    (will not copy unless colour conversion needed).

    Alternatively, :meth:`start` will activate a scheduler (thread) for writing
    device at a given frame rate given the buffered frame. The scheduler may be
    told to immediately send the frame via `notify`, otherwise it will wait
    until the next frame is due. The scheduler can be de-activated
    via :meth:`stop`.

    .. code-block:: python
        # open 640x480, BGR, 30FPS
        sink = VirtualSink('/dev/video10', 640, 480, 30)
        sink.start()
        # x = some 480x640x3 numpy array
        while True:
            sink.update_buffer(x)
            time.sleep(1 / 30)

        sink.stop()
        sink.close()

    :param device_id: identifying number of the virtual device, e.g. 0 for
         '/dev/video0'
    :type device_id: int

    :param fps: the _target_ number of frames per second to sink when using the
        scheduler

    :param in_color_code: the pixel format of incoming frames, must be one of
        :data:`IN_COLOR_CODES`, defaults to 'BGR'

    :param out_color_code: the pixel format of the loopback device, must be one
        of :data:`MAP_CV2_V4L2`, defaults to 'BGR'

    :param priority: priority to acquire the (loopback) device, defaults to
        Priority.DEFAULT, for details about camera priority
        <https://www.kernel.org/doc/html/v6.5/userspace-api/media/v4l/vidioc-g-priority.html>
    :type priority: v4l2py.device.Priority, optional
    """


    IN_COLOR_CODES = SAFE_COLOR_CODES
    """Selected formats that OpenCV can convert _from_

    For further information on color conversion in OpenCV:
    <https://docs.upencv.org/4.x/de/d25/imgproc_colour_conversions.html>
    """

    OUT_COLOR_CODES = { *MAP_CV2_V4L2 }
    """V4L2 pixel formats that OpenCV can convert _to_"""


    def __init__(self,
                 device_id: int, width: int, height: int, fps: int,
                 in_color_code: str='BGR', out_color_code: str='BGR',
                 priority: Priority=Priority.DEFAULT):

        if not device_id >= 0:
            raise need_non_negative('device id for virtual sink',
                                    device_id)

        self._stopped = True
        self._device = Device.from_id(device_id)

        self._out_format = Format(width, height, MAP_CV2_V4L2[out_color_code])
        self._fps = fps

        self.in_color_code = in_color_code
        self.open()

        self.priority = priority
        self.start()


    def __del__(self):
        self.close()


    def __call__(self):
        """Send buffered frame to device

        If the scheduler is inactive, then the buffered frame is always sent
        to the device by the call; otherwise, it will only send if the
        `send_event` is not set (which is set by the scheduler upon return, and
        cleared by any calls to `update_buffer`). In other words, the scheduler
        will only send a frame _once_ between calls to `update_buffer`.
        """

        if hasattr(self, '_buffer'):
            if self._stopped or not self.send_event.is_set():
              # allow owner call when thread is stopped, else only write if we
              # are _not_ simply over-writing
                self._buffer_manager.write(self._buffer.data)


    @property
    def in_color_code(self):
        """Pixel (colour) format of incoming frames

        If necessary, the pixel format will be copied and converted via
        `cv2.cvtColor` to the loopback device format when the buffer is updated,
        otherwise the buffer will essentially hold a 'reference' to the frame.
        Must be one of :data:`IN_COLOR_CODES`.

        :return: the `in_color_code` property of the object
        :rtype: str
        """
        return self._in_color_code


    @in_color_code.setter
    def in_color_code(self, value):

        if not value in self.IN_COLOR_CODES:
            raise RuntimeError('Input to virtual sink with colour code ' +
                               '\'{}\' [CV2] is not supported'.format(value))

        self._in_color_code = value
        self._reset_conversion()


    @property
    def out_format(self):
        """Shape and pixel (colour) format of loopback device

        The shape and pixel format of both the buffered frame and the loopback
        device. The value of the pixel format must be one of the keys
        in :data:`MAP_CV2_V4L2`, defaults to 'BGR'.

        Once a frame is sent to the loopback device, this is read-only (until
        all applications release the device): see
        <https://www.kernel.org/doc/html/v6.5/userspace-api/media/v4l/open.html#multiple-opens>

        :return: the `out_format` property of the object, which includes the
            width, height and `out_color_code`
        :rtype: tuple[int, int, str]
        """
        return self._out_format


    @out_format.setter
    def out_format(self, value: tuple[int, int, str]):

        width, height, color_code, *_ = value + ()

        if not color_code in self.OUT_COLOR_CODES:
            raise ValueError(
                'Virtual sink with colour code \'{}\''.format(color_code) +
                '[CV2] is not supported'
            )

        if not width > 0 or not height > 0:
            raise need_positive('width and height for virtual sink',
                                '{}x{}'.format(width, height))

        self._device.set_format(
            BufferType.VIDEO_OUTPUT, width, height, MAP_CV2_V4L2[color_code]
        )
        self._out_format = self._device.get_format(BufferType.VIDEO_OUTPUT)

        if self.shape != (width, height):
            raise RuntimeError('Could not set shape for virtual sink ' +
                               '({}x{} requested)'.format(width, height))
        if self.out_color_code != color_code:
            raise RuntimeError(
                'Could not set colour code for virtual sink ' +
                '(\'{}\' [CV2] '.format(color_code) + 'requested)'
            )
        self._reset_conversion()


    @property
    def out_color_code(self):
        """Pixel (color) format of loopback device

        The pixel format of both the buffered frame and the loopback device. The
        value of the pixel format must be one of the keys
        in :data:`MAP_CV2_V4L2`, defaults to 'BGR'.

        Once a frame is sent to the loopback device, this is read-only (until
        all applications release the device): see
        <https://www.kernel.org/doc/html/v6.5/userspace-api/media/v4l/open.html#multiple-opens>

        :return: the `out_color_code` property of the object
        :rtype: str
        """

        if not hasattr(self, '_out_format'):
            return None

        result = [
            str_color_code
            for (str_color_code, v4l2_color_code) in MAP_CV2_V4L2.items()
            if v4l2_color_code == self._out_format.pixel_format
        ]
        return result[0] if len(result) else None


    @property
    def shape(self):
        """Shape of the loopback device

        The width and height of both the buffered frame and the loopback device.

        Once a frame is sent to the loopback device, this is read-only (until
        all applications release the device): see
        <https://www.kernel.org/doc/html/v6.5/userspace-api/media/v4l/open.html#multiple-opens>

        :return: the `shape` property of the object, which is the width and
            height (respectively)
        :rtype: tuple[int, int]
        """
        if hasattr(self, '_out_format'):
            return (self._out_format.width, self._out_format.height)
        else:
            return None


    @property
    def fps(self):
        """Target frames per second of the loopback device

        Equivalent to the maximum time between calls from the scheduler (which
        is activated via :meth:`start`).

        :return: the `fps` property of the object.
        :rtype: int
        """
        return self._fps


    @fps.setter
    def fps(self, value):

        if not value > 0:
            raise need_positive('frame rate for virtual sink', value, 'fps')

      # v4l2 loopback FPS is application controlled so set_fps() redundant
        self._device.set_fps(BufferType.VIDEO_OUTPUT, value)

        if not self._stopped:
            self._sink_thread.period_ns = int(1E9) // value
        self._fps = value


    @property
    def priority(self):
        """Access priority of device

        The access priority should reflect how exclusively this object holds
        the device; the default allows other applications to watch the device,
        using Priority.RECORD will exclude other applications from accessing the
        device entirely.

        See
        <https://www.kernel.org/doc/html/v6.5/userspace-api/media/v4l/vidioc-g-priority.html>

        :return: the `priority` property of the object
        :rtype: v4l2py.device.Priority
        """
        return self._priority


    @priority.setter
    def priority(self, value: Priority=Priority.DEFAULT):

        self._device.set_priority(value)
        self._priority = self._device.get_priority()

        if not self._priority == value:
            raise RuntimeError('Could not set priority for virtual sink '
                               '(\'{}\' requested)'.format(value))


    @property
    def send_event(self):
        """Indicator for frame sent to device

        This event is set after control returns to the scheduler when it calls
        for a frame to be sent. Any call to `update_buffer` will clear this
        event indicator.

        :rtype: threading.Event
        """
        return self._sink_thread.call_event if not self._stopped else None


    def open(self):
        """Acquire the loopback device

        Once open, the VirtualSink can be called to send a frame to the
        device. Alternatively, use :meth:`start` to activate a scheduler which
        will call the object (to send a frame) at the specified frame rate.

        This method will close and re-acquire the device if it was already
        acquired by this application.

        :raises RuntimeError: fails if descriptor is not for a v4l2 loopback
            device
        """

        if hasattr(self, '_buffer_manager') and self._buffer_manager:
            self.close()

        self._device.open()
        driver = self._device.info.driver

        if not driver == 'v4l2 loopback':
            raise RuntimeError(
                'Virtual sink (device \'{}\') '.format(self._device.fileno()) +
                'must have driver \'v4l2_loopback\' (\'{}\' '.format(driver) +
                'detected)'
            )

        self.out_format = ( self.shape + (self.out_color_code, ) )
        self.fps = self._fps

        self._buffer_manager = BufferManager(self._device,
                                             BufferType.VIDEO_OUTPUT, size=1)


    def close(self):
        """Release the loopback device

        Will also stop the scheduler if it is running
        """

        if not self._stopped:
            self.stop()

        self._buffer_manager = None

        if hasattr(self, '_device') and self._device:
            self._device.close()


    def start(self):
        """Activate the periodic frame scheduler

        Runs a separate thread that will call the VirtualSink object at the
        desired framerate; thus sending the buffered frame to the device. Can
        be interrupted via `notify` to send a frame immediately.

        The scheduler will only send a frame once between calls to
        `update_buffer`.
        """

        if not self._stopped:
            self.stop()

        self._buffer_manager.start() # stream_on
        self._skipped_frames = 0
        self._sink_thread = WakeablePeriodic(period_ns=int(1E9) // self._fps,
                                             target=self, daemon=True)
        self._sink_thread.start()
        self._stopped = False


    def stop(self):
        """De-activate the periodic frame scheduler
        """

        if hasattr(self, '_sink_thread'):
            self._sink_thread.exit()
            del self._sink_thread

        self._buffer_manager.stop() # stream_off
        self._stopped = True


    def notify(self):
        """Interrupt the scheduler and send a frame immediately

        If the scheduler is woken up it will reset the waiting time until the
        next frame, i.e. it will not return to its previous schedule.
        """

        if not self._stopped:
            self._sink_thread.wake_condition.acquire()
            self._sink_thread.wake_condition.notify()
            self._sink_thread.wake_condition.release()


    def update_buffer(self, frame):
        """Update the buffered frame destined for loopback device

        Assigns frame to the internal `buffer` which will be sent to the
        loopback device the next time the object is called. Alternatively,
        when pixel format conversion is required, a copy is made and assigned
        in-place to the internal `buffer` in the output pixel format using
        `cv2.cvtColor`.

        :param frame: the frame to be sent to the device, as a numpy.ndarray
            with shape (height, width, channels) with channels as per the
            `in_color_code` pixel format property (8-bit data only).
        :raises TypeError: fails if numpy.dtype is not `numpy.uint8`
        :raises: ValueError: fails if input frame has incorrect shape
        """

        if not frame.dtype == np.uint8:
            raise TypeError(
                'Virtual sink needs input frame with 8-bit unsigned data ' +
                'unsigned data type (\'{}\' provided)'.format(frame.dtype)
            )

      # require input frame to have the same dimensions as output
        if not frame.shape[1::-1] == self.shape:
            raise ValueError('Virtual sink needs input frame with shape ' +
                             '{}x{} '.format(*self.shape) +
                             '([{}x{}] provided)'.format(*frame.shape[1::-1]))

        if not self._convert_code is None:
            cv2.cvtColor(frame, self._convert_code, dst=self._buffer_cvt)
        else:
            self._buffer_cvt = frame

        if not self._stopped:

            self._sink_thread.call_lock.acquire()

            if not self.send_event.is_set():
                self._skipped_frames += 1
            else:
                self.send_event.clear()

      # v4l2py "RGB32" is BA24 => need ARGB
        if self._need_move_alpha:
            n_dim = self._buffer_cvt.ndim
            j = (np.arange(0, n_dim) + 1) % n_dim
            self._buffer[:,:,j] = self._buffer_cvt
      # v4l2py "YUV32" is "YUV4" => need AYUV
        elif self._need_prepend_alpha:
            ndim = self._buffer.ndim
            j = np.arange(1, ndim)
            self._buffer[:,:,0] = 0xFF
            self._buffer[:,:,j] = self._buffer_cvt
        else:
            self._buffer = self._buffer_cvt

        if not self._stopped:
            self._sink_thread.call_lock.release()


    def _reset_conversion(self):
      # Reset pixel format conversions for input and output; will allocate
      # space for in-place conversions and additional modifications needed by
      # the 'RGBA' and 'YUV' pixel formats.
        if ( not hasattr(self, '_in_color_code') or
             not hasattr(self, '_out_format') ):
            return None

        in_color_code = self._in_color_code
        out_color_code = self.out_color_code

        self._convert_code = None

        if not in_color_code == out_color_code:
            self._convert_code = find_cv2_colorConversionCode(in_color_code,
                                                              out_color_code)
            self._buffer_cvt = np.empty(
                find_cv2_shape(self.shape[1::-1], out_color_code),
                dtype=np.uint8
            )

        self._need_move_alpha = self._need_prepend_alpha = None

        if out_color_code == 'RGBA':
            self._buffer = np.empty(self.shape[1::-1] + (4,), dtype=np.uint8)
            self._need_move_alpha = True
        elif out_color_code == 'YUV':
            self._buffer = np.empty(self.shape[1::-1] + (4,), dtype=np.uint8)
            self._need_prepend_alpha = True

        return True


# may need this later for some 4-bit depth formats
# downcast an 8-bit multi-channel format to 4-bit with half the channels
# note: video4linux pixel formats are little-endian
#
# big endian:
# 4, 3, 2, 1 --> [4 3], [2 1]
# 3, 2, 1    --> [x 3], [2 1]
# little endian:
# 1, 2, 3, 4 --> [2 1], [4 3]
# 1, 2, 3    --> [2 1], [x 3]
def _downcast_4bit(x, big_endian=False):

    n_byte = x.shape[2]
  # insert odd elements
    result = x[:,:,0:n_byte:2]
    np.right_shift(result, 4, result)
  # insert even elements
    result[:,:,0:(n_byte>>1)] += np.bitwise_and(x[:,:,1:n_byte:2], 0xF0)

    return result

