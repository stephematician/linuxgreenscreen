# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import cv2
import numpy as np

from mediapipe import Image as mp_Image

from linuxgreenscreen.green_compositor import GreenCompositor, SelfieModel
from linuxgreenscreen.mask_params import MaskParams


class BlurCompositor(GreenCompositor):
    """Blurred background using asynchronous image segmentation

    Callable compositor that blurs the background of an image (around a person)
    using image segmentation. The input frame is passed to an asynchronous
    MediaPipe segmentation task which updates the 'mask' (person vs background).
    The blur is applied immediately using the most-recently completed mask.

    In practise, this means that the blurred region is one frame 'behind' the
    input. The delay between capture and output is only a few milliseconds,
    meaning that the effect can be maintained at high frame rates.

    See also: `Compositor` and `GreenCompositor.

    :param background_blur_size: the size of the gaussian blur kernel to apply
        to the background, integer-valued and performs faster when the value is
        an odd number

    :param in_color_code: the pixel format of the image passed to the
    compositor, defaults to 'BGR'

    :param out_color_code: the pixel format of the frame that is output by each
        call, typically one of :data:`VirtualSink.IN_COLOR_CODES` in format,
        defaults to the same value as `in_color_code`

    :param selfie_model: switch for landscape (SelfieModel.LANDSCAPE) with
        144x256x3 (RGB) input tensor, or square (SelfieModel.SQUARE) with
        256x256x3 (RGB) input tensor, see
        <https://developers.google.com/mediapipe/solutions/vision/image_segmenter>,
        defaults to SelfieModel.LANDSCAPE

    :param mask_params: parameters for the updating and blending of the mask,
        more details in `MaskParams`, defaults to MaskParams()

    :param delegate_to_gpu: boolean for using GPU-based segmentation, defaults
        to False (CPU-based)
    """


    def __init__(self, background_blur_size: int,
                 width: int, height: int, in_color_code: str='BGR',
                 out_color_code: str=None,
                 selfie_model: SelfieModel=SelfieModel.LANDSCAPE,
                 mask_params: MaskParams=None,
                 delegate_to_gpu: bool=False):
        """Constructor method
        """

        super().__init__(width, height, in_color_code, out_color_code,
                         selfie_model, mask_params, delegate_to_gpu)

        self._background_blur_size = background_blur_size
        self._frame_background = np.empty(self.shape[1::-1] + (3,),
                                          dtype=np.uint8)


    def _blend_with_mask_impl(self, frame_to_blend):
        """Apply gaussian blur to background of person

        Blurs the image and then blends in the 'person' using `cv2.blendLinear`.
        The mask is a continuous 'confidence' level between 0 (background) and
        1 (person), and the blend favours the original input (non-blurred) the
        closer the mask is to 1.

        :param frame_to_blend: the image to apply the blur effect to, as a
            numpy.ndarray (or like) with `shape=(height, width, 3)` or however
            many channels the input pixel format requires

        :return: the image with blurred background, as a numpy.ndarray either
            in the same pixel format as input
        """

        need_blur = self._background_blur_size > 0

        if need_blur:
            n = self._backround_blur_size
            cv2.GaussianBlur(src=frame_to_blend, ksize=(n, n), sigmaX=n / 3,
                             dst=self._frame_background)
            self._mask_lock.acquire()
            cv2.blendLinear(src1=frame_to_blend, src2=self._frame_background,
                            weights1=self._mask, weights2=1 - self._mask,
                            dst=frame_to_blend)
            self._mask_lock.release()

        return frame_to_blend


    def _new_mask_impl(self):
        """Construct a new mask

        :return: a mask where each pixel is considered 'person' (1.0), as a
            numpy.ndarray with `shape=(height, width)` and type `np.float32`
        """
        return np.full(self.shape[1::-1], np.float32(1.0))


    def _update_mask_impl(self, latest_mask: np.ndarray, image: mp_Image,
                         timestamp_ms: int):
        """Update the current mask given segmentation task result

        Uses the latest segmentation result (`latest_mask`) to determine the
        new value of `self._mask`.

        When the compositor was constructed, a variety of steps were enabled or
        modified via the `mask_params` argument. Details are available in
        `MaskParams`. In brief, the first step is to aggregate recent
        segmentation results. The aggregated result is then dilated, blurred,
        and resized to fit the input and output frames.

        :param latest_mask: the latest result from the segmentation task as a
            numpy.ndarray with shape determined by the model, see
            :data:`MODEL_TENSOR_SHAPE`, with type `numpy.float32`.

        :param image: the input tensor passed to the segmentation task, with the
            same height and width as `latest_mask` with RGB along the final
            axis
        :type image: mediapipe.Image

        :param timestamp_ms: the timestamp of the input tensor passed to the
            segmentation task
        """
      # get the aggregate over the recent confidence masks
        self._aggregate_past(latest_mask)
      # TODO: test results when do this prior to aggregation
        if self._mask_params.dilate_size > 0:
            n = self._mask_params.dilate_size
            cv2.dilate(latest_mask,
                       np.ones((n, n), dtype=np.uint8), iterations=1,
                       dst=latest_mask)

        if self._mask_params.blur_size > 0:
            n = self._mask_params.blur_size
            cv2.blur(latest_mask, (n, n), dst=latest_mask)

        self._mask_lock.acquire()
        cv2.resize(src=latest_mask, dsize=self.shape,
                   interpolation=cv2.INTER_LINEAR, dst=self._mask)
        self._mask_lock.release()


    def _reset_conversion(self):
      # do not use GreenCompositor's intermediate conversions
        super(GreenCompositor, self)._reset_conversion()

