# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import threading
import time

from linuxgreenscreen.utility import need_non_negative


class WakeablePeriodic(threading.Thread):
    """A wakeable periodic-executing thread

    A thread class that calls its target at least as often as a sleeping period
    unless the target is locked by another process.

    :param period_ns: upper bound on the sleeping period, except when the
        target is locked, must be non-negative
    :type period_ns: int

    :param target: callable object to be invoked by run(), defaults to None
    :type target: typing.Callable, optional

    :param name: is the thread name; by befault a unique name is constructed in
        the form "Thread-N"
    :type name: str, optional

    :param args: is the argument tuple for target invocation, defaults to ()
    :type args: tuple, optional

    :param kwargs: is a dictionary of keyword arguments for the target
        invocation, defaults to {}
    :type kwargs: dict, optional

    :param daemon: whether a daemon thread or not, default is to inherit from
        the thread that creates the object
    :type daemon: bool, optional
    """

    def __init__(self, period_ns: int,
                 target=None, name=None, args=(), kwargs={}, *, daemon=None):
        """Constructor method
        """
        super().__init__(
            target=None, name=name, args=args, kwargs=kwargs, daemon=daemon
        )

        self._stopped = True
        self._periodic_target = target
        self._call_event = threading.Event()
        self._call_lock  = threading.RLock()
        self._wake_up_condition = threading.Condition(self._call_lock)

        self.period_ns = period_ns


    @property
    def call_lock(self):
        """Lock that is acquired by this when calling target

        Acquired by the thread prior to the calling the target, and released
        upon successful return from call. Can be used as a way to lock shared
        resources that the target may modify.

        :return: The `call_lock` property of the object
        :rtype: threading.RLock
        """
        return self._call_lock


    @property
    def call_event(self):
        """Target call-return event

        Set after each call of the target has returned.

        :return: The `call_event` property of the object
        :rtype: threading.Event
        """
        return self._call_event


    @property
    def wake_condition(self):
        """Waking up condition during each period

        Used to ensure an upper bound on the time between calling the target.
        User can notify in order to wake-up early and call the target at a
        higher rate.

        :return: The `wake_condition` property of the object
        :rtype: threading.Condition
        """
        return self._wake_up_condition


    @property
    def period_ns(self):
        """Period between calling target

        Must be non-negative.

        :return: the period (property) in nanoseconds
        :rtype: int
        """
        return self._period_ns


    @period_ns.setter
    def period_ns(self, value: int):

        if value < 0:
            raise need_non_negative('sleep period', value, 'ns')

        self._period_ns = value


    def run(self):
        """Run method for a wakeable periodic-executing thread
        """

        reference_ns = time.monotonic_ns()
        self._wake_up_condition.acquire()
        self._stopped = False

        while True:
          # Next (expected) target invocation occurs at the soonest (next)
          # integer period from the reference time.
            now_ns = time.monotonic_ns()
            n_advance = 1 + max(0, (now_ns - reference_ns) // self._period_ns)
            next_target_call_ns = reference_ns + n_advance * self._period_ns
          # Wait until disturbed or the next (expected) target invocation time.
            not_quietly = self._wake_up_condition.wait(
                1E-9 * (next_target_call_ns - now_ns)
            )
          # Either we are woken up in order to stop, or we need to call the
          # target NOW. In the case of the latter, we adjust the reference time
          # so that the next expected call is one period after now.
            if not_quietly:
                if self._stopped:
                    break
                reference_ns = time.monotonic_ns()
            else:
              # NOTE: Advancing reference in this case is likely redundant.
                reference_ns = next_target_call_ns
          # Invoke the call and set the event flag if anyone cares.
            self._periodic_target(*self._args, **self._kwargs)
            self._call_event.set()

        self._wake_up_condition.release()


    def exit(self):
        """Exit method for a wakeable periodic-executing thread
        """
        self._wake_up_condition.acquire()
        self._stopped = True
        self._wake_up_condition.notify()
        self._wake_up_condition.release()

