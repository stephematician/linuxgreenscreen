# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import cv2
import time
import warnings

_have_turbojpeg = False
_turbojpeg_result = None
try:
    import turbojpeg
    _have_turbojpeg = True
except Exception as e:
    _turbojpeg_result = '{}'.format(e)

import linuxgreenscreen.utility


def _is_four_upper(x):
    return len(x) == 4 and x.isalpha() and x.isupper()


def _open_camera(fd:str , width, height, fps, color_code):
    """Wrapper to acquire a camera for capture via OpenCV

    :param fd: location of camera, e.g. "/dev/video0"

    :param fps: frames per second

    :param color_code: "fourcc" code for the pixel format, e.g. "UYVY" for
        YUV 4:2:2, "MJPG" for JPEG encoded frames, see <https://fourcc.org/> for
        other codes
    """

    color_fourcc = None
    if color_code:
        color_fourcc = cv2.VideoWriter_fourcc(*(elem for elem in color_code))

    camera = cv2.VideoCapture(
        fd, cv2.CAP_V4L2,
        ( cv2.CAP_PROP_FRAME_WIDTH, width,
          cv2.CAP_PROP_FRAME_HEIGHT, height,
          cv2.CAP_PROP_FOURCC, color_fourcc )
    )
    camera.set(cv2.CAP_PROP_FPS, 30)

    return camera


class CameraSource:
    """Wrapper for webcam capture device using OpenCV

    A callable object that returns the next frame from the camera. When called
    the object will wait/block until a frame is ready for retrieval. The frame
    is then decoded (potentially using turbojpeg) and returned in BGR pixel
    format, as a cv2.Mat object.

    :param device_id: device number, e.g. 0 for a camera at "/dev/video0"

    :param width: width in pixels, defaults to the existing camera setting

    :param height: height in pixels, defaults to the existing camera setting

    :param fps: frames per second for device, defaults to the existing camera
        settinglibturbojpeg.so.0

    :param in_color_code: "fourcc" code for the pixel format, e.g. "UYVY" for
        YUV 4:2:2, "MJPG" for JPEG encoded frames, see <https://fourcc.org/> for
        other codes, defaults to the existing camera setting

    :param turbojpeg_path: path to "libjpegturbo.so.0" or equivalent (depending
        on operating system), defaults to searching some known 'default' paths
    :type turbojpeg_path: str, optional
    """


    def __init__(self,
                 device_id: int, width: int=None, height: int=None,
                 fps: int=None, in_color_code: str=None,
                 turbojpeg_path: str=None):

        if not device_id >= 0:
            raise need_non_negative('device id for source (webcam)', device_id)

        self._fd = "/dev/video{}".format(device_id)
        self._turbojpeg_path = turbojpeg_path
        self._width = width
        self._height = height
        self._fps = fps
        self._in_color_code = in_color_code

        self.open()

        self._sum_decode_ns = 0
        self._n_decode = 0


    def __del__(self):
        self.close()


    def __call__(self):
        """Get and decode a frame from the camera

        Frame is provided as a `cv2.Mat` object, with dimension (h x w x 3) in
        BGR pixel format along the final dimension.

        This call will wait (i.e. block) for a frame from the device.

        :return: (false, None) if no frame was captured, else true and the
            decoded image data
        :rtype: tuple[bool, cv2.Mat]
        """

        grabbed = self._camera.grab()
        frame = None

        if grabbed:

            start_decode_ns = time.monotonic_ns()
            grabbed, frame  = self._camera.retrieve()

            if self._using_turbojpeg:
                with warnings.catch_warnings():
                  # suppress the Logitech additional bytes warnings
                    warnings.simplefilter('ignore')
                    frame = self._turbojpeg_ptr.decode(frame.data)

            self._sum_decode_ns += time.monotonic_ns() - start_decode_ns
            self._n_decode += 1

        return grabbed, frame


    @property
    def in_color_code(self):
        """Pixel format a.k.a. color code of the camera

        The color is identified by a "fourcc" code, e.g. "UYVY" for
        YUV 4:2:2, "MJPG" for JPEG encoded frames, see <https://fourcc.org/> for
        other codes.

        :rtype: str
        """
      # Get integer representation of format code, then convert to bytes and
      # decode. NOTE: unsure about encoding rules here
        fourcc_int = int(self._camera.get(cv2.CAP_PROP_FOURCC))
        return fourcc_int.to_bytes(4, 'little').decode()


    @in_color_code.setter
    def in_color_code(self, value: str):

        if not value:
            if self.in_color_code == 'MJPG':
                self._open_turbojpeg()
            return

        if not _is_four_upper(value):
            raise ValueError('Need four-letter input colour code, e.g. ' +
                             '\'MJPG\', for webcam (source) ' +
                             '(\'{}\' requested)'.format(value))

        in_color_fourcc_gen = (elem for elem in value)

        self._camera.set(cv2.CAP_PROP_FOURCC,
                         cv2.VideoWriter_fourcc(in_color_fourcc_gen))

        if not self.in_color_code == value:
            raise RuntimeError('Input from webcam (source) with color code ' +
                               '\'{}\' [CV2] is not supported'.format(value))

        self._in_color_code = value
        self._using_turbojpeg = False

        if self.in_color_code == 'MJPG':
            self._open_turbojpeg()


    @property
    def shape(self):
        """Width and height of camera in pixels

        :return: the width and height (in that order) in pixels of captured
            images
        :rtype: tuple[int, int]
        """
        return ( self._camera.get(cv2.CAP_PROP_FRAME_WIDTH),
                 self._camera.get(cv2.CAP_PROP_FRAME_HEIGHT) )


    @shape.setter
    def shape(self, value: tuple[int, int]):

        width, height, *_ = value + ()

        if not width and not height:
            return

        if ((width and not width > 0) or (height and not height > 0)):
            raise need_positive('width and height for source (webcam)',
                                '{}x{}'.format(width, height))

        self._camera = _open_camera(self._fd,
                                    width, height, self._fps,
                                    self._in_color_code)
        post_shape = self.shape

        if ((width and not width == post_shape[0]) or
                (height and not height == post_shape[1])):
            raise RuntimeError(
                'Could not set source (webcam) shape ' +
                '({}x{} requested)'.format(width, height)
            )

        self._width = width if width else self._width
        self._height = height if height else self._height


    @property
    def fps(self):
        """Frames per second captured by camera

        :rtype: int
        """
        return self._camera.get(cv2.CAP_PROP_FPS)


    @fps.setter
    def fps(self, value: int):

        if not value:
            return

        if not value > 0:
            raise need_positive('frame rate for source (webcam)', value, 'fps')

        self._camera.set(cv2.CAP_PROP_FPS, value)

        if not self.fps == value:
            raise RuntimeError('Could not set source (webcam) frame rate ' +
                              '({}fps requested)'.format(value))

        self._fps = value


    @property
    def decode_ms(self):
        """Total milliseconds spent in decoding

        :rtype: float
        """
        return self._sum_decode_ns / 1E6 if self._sum_decode_ns else None


    @property
    def n_decode(self):
        """Number of decoded captured images

        :rtype: int
        """
        return self._n_decode


    def open(self):
        """Acquire a camera for capture via OpenCV

        :raises RuntimeError: the device must be acquired at the requested
            resolution, frames-per-second, and pixel format.
        """

        self._camera = _open_camera(self._fd,
                                    self._width, self._height, self._fps,
                                    self._in_color_code)
        post_shape = self.shape
        shape_ok = (
            (not self._width or self._width == post_shape[0]) and
            (not self._height or self._height == post_shape[1])
        )
        fps_ok = not self._fps or self._fps == self.fps
        code_ok = (
            not self._in_color_code or
            self._in_color_code == self.in_color_code
        )
        if not all((shape_ok, fps_ok, code_ok)):
            raise RuntimeError(
                'Could not open source webcam (device {} '.format(self._fd) +
                'at {}x{}, '.format(self._width, self._height) +
                '{}fps [{}] requested)'.format(self._fps, self._in_color_code)
            )

        if self.in_color_code == 'MJPG':
            self._open_turbojpeg()


    def close(self):
        """Release camera
        """
        if hasattr(self, '_camera'):
            self._camera.release()


    def _open_turbojpeg(self):

        self._turbojpeg_ptr = None

        if not _have_turbojpeg:
            warnings.warn(
                '\'import turbojpeg\' failed when linuxgreenscreen was ' +
                'imported (\'' + _turbojpeg_result + '\'): ' +
                'falling back to OpenCV JPEG decode',
                warnings.RuntimeWarning
            )
            return

        path = self._turbojpeg_path

        try:
            self._turbojpeg_ptr  = turbojpeg.TurboJPEG(path)
        except Exception as err:
            warnings.warn(
                '\'TurboJPEG({})\' did not succeed '.format(path) +
                '(\'{}\'): falling back to OpenCV JPEG decode'.format(err),
                warnings.RuntimeWarning
            )
            self._turbojpeg_ptr = None
            return

        self._camera.set(cv2.CAP_PROP_CONVERT_RGB, 0)

        if self._camera.get(cv2.CAP_PROP_CONVERT_RGB) == 0:
            self._using_turbojpeg = True
        else:
            warnings.warn('Could not disable retrieve() convert-to-RGB: ' +
                          'falling back to OpenCV JPEG decode')


# TODO: source settings class, w/- translation between V4L2 controls and OPENCV
# TODO: need to be able to update settings of the source camera somehow; start
# with some kind of ini of some sort, later, I can think about adding a GUI