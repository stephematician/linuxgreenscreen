# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import cv2
import re
from v4l2py.device import PixelFormat

#: Map from pixel formats (colour codes) in OpenCV to V4L2 pixel formats
#:
#: For some background on OpenCV colour conversion:
#: <https://docs.opencv.org/4.8.0/de/d25/imgproc_color_conversions.html>
#:
#: For the details of V4L2 pixel formats:
#: <https://www.kernel.org/doc/html/v6.5/userspace-api/media/v4l/pixfmt.html>
#:
#: For some information about identifiers for YUV formats:
#: <https://github.com/lemenkov/libyuv/blob/main/docs/formats.md>
MAP_CV2_V4L2 = {
  # PACKED FORMATS.
    'BGR': PixelFormat.BGR24, 'RGB': PixelFormat.RGB24,
    'GRAY': PixelFormat.GREY,
    'YUV': PixelFormat.YUV32, # NOTE: need to pre-pend alpha (FAILING)
  # PACKED DEPRECATED: still used by v4l2loopback
    'BGRA': PixelFormat.BGR32,
    'RGBA': PixelFormat.RGB32,  # NOTE: need to move channel 4 -> 1
  # PLANAR FORMATS.  YUV_I420 is same as IYUV; and is also know as YU12.
  # See <https://github.com/lemenkov/libyuv/blob/main/docs/formats.md>.
    'YUV_IYUV': PixelFormat.YUV420, 'YUV_I420': PixelFormat.YUV420,
    'YUV_YV12': PixelFormat.YVU420
}

SAFE_COLOR_CODES = (
    'BGR', 'BGRA', 'GRAY', 'Luv', 'RGB', 'RGBA', 'YCrCb', 'YUV', 'YUV_I420',
    'YUV_IYUV', 'YUV_NV12', 'YUV_NV21', 'YUV_UYNV', 'YUV_UYVY', 'YUV_Y422',
    'YUV_YUNV', 'YUV_YUY2', 'YUV_YUYV', 'YUV_YV12', 'YUV_YVYU', 'YUV420p',
    'YUV420sp'
)
"""Selected formats that OpenCV can convert _from_

For further information on color conversion in OpenCV:
<https://docs.upencv.org/4.x/de/d25/imgproc_colour_conversions.html>
"""

def find_cv2_colorConversionCode(in_color_code: str, out_color_code: str):
    """Find conversion code via search of cv2 module

    Based on <https://docs.opencv.org/4.x/df/d9d/tutorial_py_colorspaces.html>

    :param in_color_code: codes usually from the keys of `MAP_CV2_V4L2`, e.g.
        'RGB', 'YUV_I420', ...

    :raises ValueError: fails when no unique code found

    :return: colour conversion code recognised by :meth:`cv2.cvtColor`
    :rtype: integer
    """
  # input code must be in format '<in_prefix>_<in_suffix>'
    in_prefix = re.sub('^([^_]+)_?[^_]*', r'\1', in_color_code)
    in_suffix = re.sub('^' + in_prefix + '(_?[^_]*)', r'\1', in_color_code)
  # conversion code must start with COLOR_<in_prefix>2<out_fmt>_<in_suffix>
    regex = (
        '^COLOR_' + in_prefix + '2' + out_color_code + '_?' + in_suffix + '$'
    )
    code = [ nm for nm in dir(cv2) if re.search(regex, nm) ]
  # check for suffix and filter
    if len(code) == 1:
        return getattr(cv2, code[0])
    else:
        raise ValueError(
            'Could not find unique code for conversion from code ' +
            '\'{}\' [CV2] to code '.format(in_color_code) +
            '\'{}\' [CV2]'.format(out_color_code)
        )


def find_cv2_shape(shape: tuple[int, int], color_code: str):
    """Find array shape given pixel format

    :type shape: tuple[int, int]

    :param color_code: codes usually from the keys of `MAP_CV2_V4L2`, e.g.
        'RGB', 'YUV_I420', ...

    :return: tuple giving the shape of the array needed by cv2 for an image of
        the color code requested
    """

    if color_code in ('BGR', 'RGB', 'HLS', 'HSV', 'LAB', 'LUV',
                      'Lab', 'Luv', 'XYZ', 'YCR_CB', 'YCrCb', 'YUV',
                      'HLS_FULL', 'HSV_FULL'):
        return shape + (3, )
    elif color_code in ('BGRA', 'RGBA'):
        return shape + (4, )
    elif color_code in ('GRAY'):
        return shape
  # potential 4:2:2 and 4:2:0 sub-sampling or bgr555/565; use a 2x2 block to
  # check
    test_cvt = cv2.cvtColor(
        cv2.numpy.empty((2, 2, 3), dtype=cv2.numpy.uint8),
        find_cv2_colorConversionCode('RGB', color_code)
    )
    cvt_scale = tuple(v / 2 for v in test_cvt.shape[:2])
    cvt_channel = test_cvt.shape[2:]

    return tuple(int(v * a) for v, a in zip(shape, cvt_scale)) + cvt_channel

