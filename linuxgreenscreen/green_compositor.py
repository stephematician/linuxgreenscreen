# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import cv2
import itertools
import numpy as np

from collections import deque

from mediapipe import Image as mp_Image

from linuxgreenscreen.compositor import Compositor, SelfieModel
from linuxgreenscreen.conversion_code import find_cv2_shape
from linuxgreenscreen.conversion_code import find_cv2_colorConversionCode
from linuxgreenscreen.mask_params import MaskParams


class GreenCompositor(Compositor):
    """Green screen around a person using asynchronous image segmentation

    Callable compositor that sets the background of an image to green using
    image segmentation. The input frame is passed to an asynchronous MediaPipe
    segmentation task which updates the 'mask' (person vs background). The green
    screen effect is applied immediately using the most-recently completed mask.

    In practise, this means that the position of the green screen is one frame
    'behind' the input. The delay between capture and output is only a few
    milliseconds, meaning that the effect can be maintained at high frame rates.

    :param in_color_code: the pixel format of the image passed to the
    compositor, defaults to 'BGR'

    :param out_color_code: the pixel format of the frame that is output by each
        call, typically one of :data:`VirtualSink.IN_COLOR_CODES` in format,
        defaults to the same value as `in_color_code`

    :param selfie_model: switch for landscape (SelfieModel.LANDSCAPE) with
        144x256x3 (RGB) input tensor, or square (SelfieModel.SQUARE) with
        256x256x3 (RGB) input tensor, see
        <https://developers.google.com/mediapipe/solutions/vision/image_segmenter>,
        defaults to SelfieModel.LANDSCAPE

    :param mask_params: parameters for the updating and blending of the mask,
        more details in `MaskParams`, defaults to MaskParams()

    :param delegate_to_gpu: boolean for using GPU-based segmentation, defaults
        to False (CPU-based)
    """


    def __init__(
        self, width: int, height: int, in_color_code: str='BGR',
        out_color_code: str=None,
        selfie_model: SelfieModel=SelfieModel.LANDSCAPE,
        mask_params: MaskParams=None,
        delegate_to_gpu: bool=False
    ):
        super().__init__(width, height, in_color_code, out_color_code,
                         selfie_model, mask_params, delegate_to_gpu)
      # TODO: use a queue that will recycle memory instead
        self._mask_is_person = None
        self._mask_small = None
        self._mask_buffer = deque(maxlen=self._mask_params.n_aggregate)


    def _blend_with_mask_impl(self, frame_to_blend):
        """Apply the 'green screen' effect around person

        Sets pixels in the green channel to maximum (and other channels to zero)
        where the mask is zero (background).

        If the input does not have a green channel (i.e. not 'RGB' or 'BGR') it
        will be converted to 'RGB' or 'BGR' (depending upon output pixel format)
        and the conversion within `blend_with_mask` will be suitably adjusted
        via the `_reset_conversion` method of _this_ class.

        :param frame_to_blend: the image to apply green screen to, as a
            numpy.ndarray (or like) with `shape=(height, width, 3)` or however
            many channels the input pixel format requires

        :return: the image with green values in place of the background, as a
            numpy.ndarray either in 'RGB' or 'BGR' pixel format (depending on
            output pixel format)
        """

        if not self._pre_blend_convert_code is None:
          # use storage
            frame_blended = self._frame_blended
            cv2.cvtColor(frame_to_blend, self._pre_blend_convert_code,
                         dst=frame_blended)
        else:
          # in-place
            frame_blended = frame_to_blend

        self._mask_lock.acquire()

        if self._mask is None:
            self._mask = self._new_mask_impl()
            self._mask_is_person = self._mask + 1

      # place green pixels over background using 8-bit values:
      #  -   `mask`      : 255=background; 0=person
      #  -   `is_person` :   0=background; 1=person
        frame_blended[:,:,0] *= self._mask_is_person
        frame_blended[:,:,1] |= self._mask            # green
        frame_blended[:,:,2] *= self._mask_is_person

        self._mask_lock.release()

        return frame_blended


    def _new_mask_impl(self):
        """Construct a new mask

        :return: a mask where each pixel is considered 'person' (255), as a
            numpy.ndarray with `shape=(height, width)` and type `np.uint8`
        """
        return np.full(self.shape[1::-1], np.uint8(255))


    def _update_mask_impl(self, latest_mask: np.ndarray, image: mp_Image,
                          timestamp_ms: int):
        """Update the current mask given segmentation task result

        Uses the latest segmentation result (`latest_mask`) to determine the
        new value of `self._mask`, and `self._mask_is_person`.

        When the compositor was constructed, a variety of steps were enabled or
        modified via the `mask_params` argument. Details are available in
        `MaskParams`. In brief, the first step is to aggregate recent
        segmentation results. The aggregated result is then dilated, blurred,
        then converted into 8-bit values (i.e. from 0 .. 255) and resized to
        fit the input and output frames.

        The final step is to apply a threshold to create the two values needed
        for the green screen. `self._mask` has `0` for background and `255` for
        person, while `self._mask_is_person` is `0` and `1`, respectively.

        :param latest_mask: the latest result from the segmentation task as a
            numpy.ndarray with shape determined by the model, see
            :data:`MODEL_TENSOR_SHAPE`, with type `numpy.float32`.

        :param image: the input tensor passed to the segmentation task, with the
            same height and width as `latest_mask` with RGB along the final
            axis
        :type image: mediapipe.Image

        :param timestamp_ms: the timestamp of the input tensor passed to the
            segmentation task
        """
      # get the aggregate over the recent confidence masks
        self._aggregate_past(latest_mask)
      # TODO: test results when do this prior to aggregation
        if self._mask_params.dilate_size > 0:
            n = self._mask_params.dilate_size
            cv2.dilate(latest_mask,
                       np.ones((n, n), dtype=np.uint8), iterations=1,
                       dst=latest_mask)

        if self._mask_params.blur_size > 0:
            n = self._mask_params.blur_size
            cv2.blur(latest_mask, (n, n), dst=latest_mask)

      # now acquire [0..255] byte array and use threshold to
      # get 8-bit `mask` with values  : 255=background; 0=person.
      # `is_person=mask+1` has values :   0=background; 1=person.
        latest_mask *= 255

        if self._mask_small is None:
          # make new small mask
            self._mask_small = latest_mask.astype(np.uint8)
        else:
          # convert by assign-in-place - marginally faster
            self._mask_small[:] = latest_mask

        self._mask_lock.acquire()

        cv2.resize(src=self._mask_small, dsize=self.shape,
                   interpolation=cv2.INTER_LINEAR, dst=self._mask)

        if not self._mask_params.threshold == 0:
            cv2.threshold(self._mask, 255 * self._mask_params.threshold, 255,
                          type=cv2.THRESH_BINARY_INV, dst=self._mask)

      # TODO: check if numpy elision would work better here; i.e.
      # self._mask_is_person = self._mask + 1 (have to look at stack)
        if self._mask_is_person is None:
            self._mask_is_person = self._mask + 1
        else:
            np.copyto(self._mask_is_person, self._mask)
            self._mask_is_person += 1

        self._mask_lock.release()


    def _aggregate_past(self, latest_mask: np.ndarray):
      # Aggregates the deque that holds previous segmentation task results,
      # where the older results are reduced by the factor `decay rate` each
      # time a new result is appended. The aggregate opereration used is the
      # (per pixel) maximum.
        need_aggregate = (self._mask_params.n_aggregate > 1 and
                              self._mask_params.decay_rate > 0)

      # cumulative maximum over mask queue w/- "in-place" decay applied queue
        if need_aggregate:

            self._mask_lock.acquire()
            self._mask_buffer.append(latest_mask.copy())

            for each_mask in itertools.islice(self._mask_buffer, 1, None):
                each_mask *= self._mask_params.decay_rate
                cv2.max(each_mask, latest_mask, dst=latest_mask)

            self._mask_lock.release()


    def _reset_conversion(self):
      # Inserts a conversion to RGB or BGR if needed so that the green channel
      # can be easily accessed when blending; will update the output conversion
      # if the output pixel format is not (also) RGB or BGR.
        super()._reset_conversion()

        need_pre_blend_convert = not self._in_color_code in ('RGB', 'BGR')
        pre_blend_code = (
            self._out_color_code if self._out_color_code in ('RGB', 'BGR') else
            'BGR'
        )
        need_output_convert = pre_blend_code != self._out_color_code

        if need_pre_blend_convert:
          # get conversion code and allocate blended frame workspace
            self._pre_blend_convert_code = find_cv2_colorConversionCode(
                self._in_color_code, pre_blend_code
            )
            self._frame_blended = np.empty(self.shape[1::-1] + (3,),
                                           dtype=np.uint8)
        else:
          # in-place blend workspace  - no storage
            self._pre_blend_convert_code = self._frame_blended = None

        if need_output_convert:
          # get conversion code and allocate output frame workspace
            self._output_convert_code = find_cv2_colorConversionCode(
                pre_blend_code, self._out_color_code
            )
            self._frame_out = np.empty(
                find_cv2_shape(self.shape[1::-1], self._out_color_code),
                dtype=np.uint8
            )
        else:
          # in-place output frame - no storage
            self._output_convert_code = self._frame_out = None


    def _reset_mask(self):
        super()._reset_mask()
        self._mask_lock.acquire()
        self._mask_is_person = None
        self._mask_small = None
        if hasattr(self, '_mask_buffer'):
            self._mask_buffer.clear()
        self._mask_lock.release()

