# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

from dataclasses import dataclass

import linuxgreenscreen.utility

@dataclass
class MaskParams:
    """Parameters for processing segmentation results into a mask.

    Current and past segmentation results are blended together and
    post-processed to construct a binary 'mask' which indicates where to put
    the green screen.

    The process is broken into aggregation (of current and past results), a
    smoothing step, and then a simple threshold to determine the green/non-green
    parts of the mask. See the members of this class for details about the
    parameters of each step.

    :exclude_members: dataclass
    """

    period: int = 1
    """Number of frames per mask acquisition; must be non-zero. For example,
    period=2 means the mask is updated every other frame
    """
    n_aggregate: int = 2
    """Number of segmentation results to aggregate. n_aggregate=0 or =1 for no
    aggregation
    """
    decay_rate: float = 0.9
    """Decay rate to apply to older segmentation results when aggregating
    """
    dilate_size: int = 0
    """Kernel size to dilate the aggregated results, increases the probability
    in regions where the gradient in probability is high. 0 for none
    """
    blur_size: int = 7
    """The kernel size for a box filter applied to the aggregated results,
    softens steep gradients in probability. 0 for none
    """
    threshold: float = 0.20
    """Threshold for binary classification of 'person' versus 'background' given
    the aggregated, dilated, and blurred results.
    """

    def __post_init__(self):

        if not self.period > 0:
            raise need_positive('')
            raise ValueError(
                'Need positive value of mask period ({}' +
                '{} requested)'.format(self.period)
            )

        if not self.n_aggregate >= 0:
            raise ValueError(
                'Need a non-negative number of masks to aggregate ' +
                '({} requested)'.format(self.n_aggregate)
            )

        if not self.n_aggregate >= 0 and not self.decay_rate <= 1:
            raise ValueError(
                'Need a multiplicative decay rate for aggregation in ' +
                'the range [0,1] ({} requested)'.format(self.decay_rate)
            )

        if not self.dilate_size >= 0:
            raise ValueError(
                'Need a non-negative dilation window size ' +
                '({} requested)'.format(self.dilate_size)
            )

        if not self.blur_size >= 0:
            raise ValueError(
                'Need a non-negative blurring kernel size ' +
                '({} requested)'.format(self.blur_size)
            )

        if not self.threshold >= 0 and not self.threshold <= 1:
            raise ValueError(
                'Need threshold for binary classification in the range [0,1] ' +
                '({} requested)'.format(self.threshold)
            )

