# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

import cv2
import numpy as np
import os
import threading
import time
import warnings

from enum import Enum

from mediapipe import Image as mp_Image
from mediapipe import ImageFormat as mp_ImageFormat
from mediapipe.tasks import python as mp_python
from mediapipe.tasks.python import vision as mp_vision
from mediapipe.tasks.python.vision.image_segmenter import ImageSegmenterResult as mp_ImageSegmenterResult

import linuxgreenscreen.utility
from linuxgreenscreen.conversion_code import find_cv2_shape, SAFE_COLOR_CODES
from linuxgreenscreen.conversion_code import find_cv2_colorConversionCode
from linuxgreenscreen.mask_params import MaskParams


class SelfieModel(Enum):
    SQUARE    = 0
    LANDSCAPE = 1


MODEL_TENSOR_SHAPE = {
    SelfieModel.SQUARE: (256, 256),
    SelfieModel.LANDSCAPE: (144, 256)
}


class Compositor:
    """Abstract compositor using asynchronous MediaPipe image segmentation

    This callable takes an input frame for segmentation and composition. The
    frame is passed to an asynchronous MediaPipe segmentation task which updates
    the compositor's 'mask'. After passing the frame the segmentation task, the
    frame is immediately blended with the most recently completed mask and
    returned by the compositor in the (desired) output pixel format.

    In practise, this means the composition uses a mask that is one or two
    frames 'behind' the input. The delay between capture and output depends only
    on the time taken to blend.

    This is an abstract class that requires inheritors to implement:

    -   `_update_mask_impl`: updates `self._mask` given the output of the
        segmentation task.
    -   `_blend_with_mask_impl`: composites (blends) the input frame and the
        immediately available value of `self._mask`.

    Before being passed to the segmentation task, the input frame is shrunk to
    the segmentation model's input tensor size using :func:`cv2.resize` and then
    converted to RGB pixel format. The output of the segmentation task will
    therefore have the same height and width as the input tensor, i.e. 144x256
    for the 'landscape' model (see `selfie_model` for details).

    The input frame is passed as-is to `_blend_with_mask_impl` and should be
    returned in the same pixel format; the conversion to the pixel format for
    output is performed by this class.

    Both procedures above should lock and unlock `self._mask_lock` before
    modification of `self._mask` to avoid concurrent access.

    See `GreenCompositor` for an example of an implementation.

    :param in_color_code: the pixel format of the image passed to the
    compositor, defaults to 'BGR'

    :param out_color_code: the pixel format of the frame that is output by each
        call, typically one of :data:`VirtualSink.IN_COLOR_CODES` in format,
        defaults to the same value as `in_color_code`
    :type out_color_code: str, optional

    :param selfie_model: switch for landscape (SelfieModel.LANDSCAPE) with
        144x256x3 (RGB) input tensor, or square (SelfieModel.SQUARE) with
        256x256x3 (RGB) input tensor, see
        <https://developers.google.com/mediapipe/solutions/vision/image_segmenter>,
        defaults to SelfieModel.LANDSCAPE

    :param mask_params: parameters for the updating and blending of the mask,
        more details in `MaskParams`, defaults to MaskParams()
    :type mask_params: MaskParams, optional

    :param delegate_to_gpu: boolean for using GPU-based segmentation, defaults
        to False (CPU-based)
    """


    def __init__(
        self, width: int, height: int, in_color_code: str='BGR',
        out_color_code: str=None,
        selfie_model: SelfieModel=SelfieModel.LANDSCAPE,
        mask_params: MaskParams=None,
        delegate_to_gpu: bool=False
    ):

        self._stopped = True
        self._width = width
        self._height = height
        self._selfie_model = selfie_model
        self._delegate = (
            mp_python.BaseOptions.Delegate.GPU if delegate_to_gpu else
            mp_python.BaseOptions.Delegate.CPU
        )

        self._in_color_code = in_color_code
        self._out_color_code = out_color_code

        if mask_params is None:
            mask_params = MaskParams()

        self._mask_params = mask_params
        self._mask = None
        self._mask_lock  = threading.Lock()

        self.open()


    def __call__(self, frame, j_frame: int=0, use_copy: bool=False):
        """Compose and asynchronously segment an input image

        The input frame is resized and converted to RGB pixel format before it
        is passed to the asynchronous segmentation task via
        `segmenter.segment_async`. The task will call `_update_mask` with the
        result. The segmentation task is used every `_mask_params.period` frames
        if the call included a (frame) counter `j_frame`; when
        `j_frame % period = 0` the input will be passed to the segmentation
        task.

        After the input tensor has been passed to the segmentation task, the
        input _frame_ is immediately passed to `_blend_with_mask` which is a
        wrapper around the `_blend_with_mask_impl` interface. The wrapper
        performs the final conversion from the input pixel format to the output
        format (see :meth:`_blend_with_mask`).

        :param frame: a captured image for composition, a numpy.ndarray (-like)
            with `shape=(height, width, 3)` or however many channels the input
            pixel format requires

        :param j_frame: a counter to identify which frames should be passed to
            the asynchronous segmentation task (see above), defaults to 0 i.e.
            degenerately pass the frame to the task regardless of
            `_mask_params.period`

        :param use_copy: whether to copy the input prior to blending, defaults
            to False

        :raises RuntimeError: fails if the dimensions of the input do not match
            the compositors

        :return: an image of the same size as the input frame in the pixel
            format given by the `out_color_code` property
        :rtype: numpy.ndarray
        """

        if frame.shape[1::-1] != self.shape:
            raise ValueError(
                'Compositor needs input with shape {}x{}'.format(*self.shape),
                '({}x{} provided)'.format(*frame.shape[1::-1])
            )

        enqueue_segmentation = (j_frame % self._mask_params.period) == 0

        if enqueue_segmentation and not self._stopped:

            self._start_segment_ns = time.monotonic_ns()
            cv2.resize(frame, dsize=self._input_tensor.shape[1::-1],
                       dst=self._input_tensor,
                       interpolation=cv2.INTER_NEAREST_EXACT)

            if not self._tensor_convert_code is None:
                cv2.cvtColor(self._input_tensor, self._tensor_convert_code,
                             dst=self._input_tensor_rgb)
            else:
                self._input_tensor_rgb = self._input_tensor

            self._segmenter.segment_async(
                image=mp_Image(mp_ImageFormat.SRGB,
                               data=self._input_tensor_rgb),
                timestamp_ms=int(self._start_segment_ns / 1E6)
            )

        frame_to_blend = frame if not use_copy else frame.copy()

        return self._blend_with_mask(frame_to_blend)


    def __del__(self):
        self.close()


    @property
    def shape(self):
        """Shape of input and output frames for compositor

        :return: the (width, height) property of the compositor
        :rtype: tuple[int, int]
        """
        return (self._width, self._height)


    @shape.setter
    def shape(self, value: tuple[int, int]):

        self._width, self._height, *_ = value + ()

        if not self._width > 0 or not self._height > 0:
            raise need_positive('width and height for compositor',
                                '{}x{}'.format(width, height))

        self._reset_conversion()


    @property
    def color_codes(self):
        """Input and output pixel format (a.k.a. colour code) pair.

        The pixel format codes for the input frames and output frames,
        respectively, as a pair (tuple).  If needed, conversion  is performed
        via :func:`cv2.cvtColorCode` after the composition (blending) step. An
        additional conversion is required for the tensor passed to the
        segmentation task if the input format is not 'RGB'.

        :return: the `_in_color_code` and `_out_color_code` properties
        :rtype: str
        """
        return self._in_color_code, self._out_color_code


    @color_codes.setter
    def color_codes(self, value: tuple[str, str]):

        self._in_color_code, self._out_color_code, *_ = value + ()

        if self._out_color_code is None:
            self._out_color_code = self._in_color_code

        if not self._in_color_code in SAFE_COLOR_CODES:
            raise ValueError('Input to compositor with colour code ' +
                             '\'{}\' [CV2] is not supported'.format(value))
        if not self._out_color_code in SAFE_COLOR_CODES:
            raise ValueError('Compositor with colour code ' +
                             '\'{}\' [CV2] is not supported'.format(value))

        self._reset_conversion()


    @property
    def segment_ms(self):
        """Milliseconds used by the segmentation task

        The total time from preparing the input tensor through to receiving the
        result from the segmenter.

        :return: milliseconds or None if no segmentation tasks executed
        :rtype: float
        """
        return (
            self._sum_segment_ns / 1E6 if self._sum_segment_ns else None
        )


    @property
    def n_segment(self):
        """Number of segmentation tasks executed
        """
        return self._n_segment


    @property
    def blend_ms(self):
        """Milliseconds used by blending (compositing) steps

        The total time spent blending the input frame with the mask and
        converting to the output frame.

        :return: milliseconds or None if no blending steps performed
        :rtype: float
        """
        return (
            self._sum_blend_ns / 1E6 if self._sum_blend_ns else None
        )


    @property
    def n_blend(self):
        """Number of blending steps performed
        """
        return self._n_blend


    def open(self):
        """Acquire segmentation task resources

        The constructor method will call :meth:`open` to initialise the
        segmentation task resources, reset colour conversion properties, and
        the counters for the time spent in segmentation and blending.

        The user can close these resources via :meth:`close`; after which all
        input to the compositor (call) will be returned unaltered except for
        possible conversion of pixel format.

        :raises ValueError: fails if selfie_model is not one of the enumerated
            values in `SelfieModel`
        """

        if not self._stopped:
            self.close()

        if self._selfie_model == SelfieModel.LANDSCAPE:
            selfie_tflite = 'selfie_segmenter_landscape.tflite'
        elif self._selfie_model == SelfieModel.SQUARE:
            selfie_tflite = 'selfie_segmenter.tflite'
        else:
            raise ValueError(
                'Need compositor \'selfie_model\' one of SelfieModel.SQUARE ' +
                'or SelfieModel.LANDSCAPE ({} requested)'.format(selfie_model)
            )

        model_asset_path = os.path.join(os.path.dirname(__file__), 'models',
                                        selfie_tflite)
        base_options = mp_python.BaseOptions(model_asset_path=model_asset_path,
                                             delegate=self._delegate)

      # closure to pass as the livestream callback
        def update_mask_closure(result: mp_ImageSegmenterResult,
                                image: mp_Image,
                                timestamp_ms: int):

            if self._stopped:
                return

            expected_ms = int(self._start_segment_ns / 1E6)

            if timestamp_ms and timestamp_ms != expected_ms:
                warnings.warn(
                    'Compositor dropped one or more frames prior to frame ' +
                    'with timestamp = {}ms'.format(expected_ms),
                    warnings.RuntimeWarning
                )

            self._update_mask(result, image, timestamp_ms)

        options = mp_vision.ImageSegmenterOptions(
            base_options=base_options,
            running_mode=mp_vision.RunningMode.LIVE_STREAM,
            output_category_mask=False,
            output_confidence_masks=True,
            result_callback=update_mask_closure
        )
        self._segmenter = mp_vision.ImageSegmenter.create_from_options(options)
        self._input_tensor = np.empty(
            find_cv2_shape(MODEL_TENSOR_SHAPE[self._selfie_model],
                            self._in_color_code),
            dtype=np.uint8
        )

        self._reset_conversion()
        self._reset_counter()
        self._reset_mask()
        self._stopped = False


    def close(self):
        """Release segmentation task resources

        After the resources are released; segmentation, mask updates, and
        compositing are no longer performed when the compositor is called;
        input will be return unaltered except for possible pixel format
        conversion.

        The resources can be re-opened via :meth:`open`.
        """

        self._stopped = True

        if hasattr(self, '_input_tensor'):
            del self._input_tensor

        if hasattr(self, '_segmenter'):
            self._segmenter.close()
            del self._segmenter

        self._reset_mask()


    def _blend_with_mask(self, frame_to_blend):
        """Wrapper for blending input with current mask

        Blends input via the `blend_with_mask_impl` interface; passing the
        input frame, and expects output ready for conversion into the output
        pixel format.

        `blend_with_mask_impl` should protect itself from concurrent changes to
        `_mask`, and the `_mask` resource may not persist between calls.

        If the compositor has been closed, then blending is skipped.

        :param frame_to_blend: a numpy.ndarray (-like) with
            `shape=(height, width, 3)` or however many channels the input pixel
            format requires

        :return: the composited (if needed) image in the output pixel format
        :rtype: numpy.ndarray
        """

        _start_blend_ns = time.monotonic_ns()

        if not self._stopped:
            frame_blended = self._blend_with_mask_impl(frame_to_blend)
        else:
            frame_blended = frame_to_blend

        if not self._output_convert_code is None:
            frame_out = self._frame_out
            cv2.cvtColor(frame_blended, self._output_convert_code,
                         dst=frame_out)
        else:
            frame_out = frame_blended

        self._sum_blend_ns += time.monotonic_ns() - _start_blend_ns
        self._n_blend += 1

        return frame_out


    def _update_mask(self, result: mp_ImageSegmenterResult, image: mp_Image,
                     timestamp_ms: int):
        """Wrapper for updating the current mask given segmentation result

        :param result: the result of a call to MediaPipe's `segment_async`, see
            <https://developers.google.com/mediapipe/api/solutions/python/mp/tasks/vision/ImageSegmenter>
        :type result: mediapipe.tasks.python.vision.image_segmenter.ImageSegmenterResult

        :param image: the input tensor passed to the segmentation task, with the
            shape determined by the model; see :data:`MODEL_TENSOR_SHAPE`
        :type image: mediapipe.Image

        :param timestamp_ms: the timestamp of the input tensor passed to the
            segmentation task
        """

        if self._stopped:
            return

        self._mask_lock.acquire()

        if self._mask is None:
            self._mask = self._new_mask_impl()

        self._mask_lock.release()

        latest_mask = result.confidence_masks[0].numpy_view().astype(
            np.float32, copy=True
        )

        self._update_mask_impl(latest_mask, image, timestamp_ms)

        self._sum_segment_ns += time.monotonic_ns() - self._start_segment_ns
        self._n_segment += 1


    def _reset_counter(self):
      # Reset performance counters.
        self._sum_segment_ns = 0
        self._n_segment = 0
        self._sum_blend_ns = 0
        self._n_blend = 0


    def _reset_conversion(self):
      # Reset pixel format conversions for the input tensor and the output
      # frame. Will also allocate space in order to do in-place operations when
      # converting from one pixel format to another.
        need_tensor_convert = self._in_color_code != 'RGB'
        need_output_convert = self._in_color_code != self._out_color_code

        if need_tensor_convert:
          # get conversion code and allocate tensor-in-RGB (resized) workspace
            self._tensor_convert_code = find_cv2_colorConversionCode(
                self._in_color_code, 'RGB'
            )
            self._input_tensor_rgb = np.empty(
                self._input_tensor.shape[:2] + (3,), dtype=np.uint8
            )
        else:
          # in-place tensor (resized) - no extra storage needed
            self._tensor_convert_code = self._input_tensor_rgb = None

        if need_output_convert:
          # get conversion code and allocate output frame workspace
            self._output_convert_code = find_cv2_colorConversionCode(
                self._in_color_code, self._out_color_code
            )
            self._frame_out = np.empty(
                find_cv2_shape(self.shape[1::-1], self._out_color_code),
                dtype=np.uint8
            )
        else:
          # in-place output frame - no storage
            self._output_convert_code = self._frame_out = None


    def _reset_mask(self):
      # Release the mask's resources.
        self._mask_lock.acquire()
        self._mask = None
        self._mask_lock.release()

