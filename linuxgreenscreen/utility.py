# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

def need_non_negative(name, value, unit=''):
    return ValueError('Need non-negative value of ' + x +
                      ' ({}{} requested)'.format(value, unit))

def need_positive(name, value, unit=''):
    return ValueError('Need positive-valued ' + name +
                      '({}{} requested)'.format(value, unit))

