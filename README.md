<!-- SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
     SPDX-License-Identifier: MIT
 -->

linuxgreenscreen
================

An OpenCV ([opencv-python][opencv_python_gh]) and [MediaPipe][mediapipe_gh]
-based 'fake' green screen for webcams with support for CPU- or GPU-based
decoding and segmentation. Provides a wrapper for OpenCV (webcam) capture, a
compositor using Google's MediaPipe, and writing to a virtual video device
created by [v4l2loopback][v4l2loopback_wiki] via the [v4l2py][v4l2py_gh] module.

These scripts and module(s) are inspired by and based on the work of [Fufu
Fang](mailto:fangfufu2003@gmail.com) who wrote the
[Linux-Fake-Background-Webcam][linux_fake_bg_webcam_gh] scripts.

stephematician humbly stands on the shoulders of [many
giants](#acknowledgements). Here's an early version on stream using [OBS's
Chroma Key][obs_chroma_url] filter:

<img src="./doc/stephematishun_stream_demo.webp" alt="animated image of stream using green screen" title="linuxgreenscreen" width="440"/>

[linux_fake_bg_webcam_gh]: https://github.com/fangfufu/Linux-Fake-Background-Webcam
[mediapipe_gh]: https://github.com/google/mediapipe
[obs_chroma_url]: https://obsproject.com/kb/chroma-key-filter
[opencv_python_gh]: https://github.com/opencv/opencv-python
[v4l2loopback_wiki]: https://github.com/umlaeute/v4l2loopback/wiki
[v4l2py_gh]: https://github.com/tiagocoutinho/v4l2py


# Features

So, why use this over the mighty [OBS background removal
plugin][obs_bg_removal_gh], the OG [Fake Background
Webcam][linux_fake_bg_webcam_gh], or [backscrub][backscrub_gh]?

The two major reasons are:

-   ⌚ _Asynchronous and scalable overlay processing_: CPU/GPU load can be
    simply scaled for a small cost in recency (and visual quality).
-   🐍 _It's all python3_: Easily customise the GreenCompositor class to
    add your own effects.

Minor or cosmetic reasons include:

-   Uses the latest [ImageSegmentation API][mh_image_api_url] from MediaPipe -
    GPU segmentation possible (see [§GPU](#gpu) below).
-   Faster JPEG decoder via [PyTurboJPEG][pyturbojpeg_gh] (almost 2x faster w/-
    50% less CPU usage than OpenCV in tests).
-   Additional input and output colour formats supported (e.g. YUV 420, MJPG,
    RGB, BGR).

[backscrub_gh]: https://github.com/floe/backscrub
[mh_image_api_url]: https://developers.google.com/mediapipe/solutions/vision/image_segmenter
[obs_bg_removal_gh]: https://github.com/royshil/obs-backgroundremoval
[pyturbojpeg_gh]: https://github.com/lilohuang/PyTurboJPEG


# Installation

For CPU-based segmentation, installation of the demo is straightforward using
git:

```sh
# demo requires git to install
pip install git+https://gitlab.com/stephematician/linuxgreenscreen.git
```

Details for enabling GPU-based segmentation are provided below in [§GPU](#gpu).


# Dependencies

We'll need the [v4l2loopback][v4l2loopback_wiki] module installed on the system:

```sh
# debian-based linux packages
apt install v4l2loopback-dkms
# optional
apt install v4l2loopback-utils # for v4l2-ctl
apt install ffmpeg             # debugging
```


# Demonstration

## CPU-based virtual green screen

A demo is available in [./linuxgreenscreen/demo](./linuxgreescreen/demo) that
uses `/dev/video0` as input at 1280x720 at 30fps \[MJPG\], and outputs to a
loopback device at `/dev/video10` in BGR. We first create a loopback device via
the (shell) terminal:

```sh
sudo modprode v4l2loopback devices=1 video_nr=10
for I in /sys/class/video4linux/video*; do echo "$I $(cat $I/name)"; done
# we should see ...
#   /sys/class/video4linux/video10 Dummy video device (0x0000)
```

We run the demo via python:

```sh
python3 -m linuxgreenscreen.demo
# press Ctrl-C in the terminal to quit
```

We can monitor the virtual device with [`ffplay`][ffplay_url] in another
terminal:

```sh
ffplay /dev/video10
# Ctrl-C to quit ffplay
```

Finally, we can remove the virtual device back in the terminal:

```sh
sudo rmmod v4l2loopback
```

[ffplay_url]: https://ffmpeg.org/ffplay.html


# GPU

MediaPipe is not built with GPU-based image segmentation enabled by default.
We'll need to build MediaPipe with `MEDIAPIPE_DISABLE_GPU=0`. The official
documentation provides some [instructions][mediapipe_install_url] and I have
some additional notes for Ubuntu in [INSTALL_GPU.md](./INSTALL_GPU.md).

[mediapipe_install_url]: https://developers.google.com/mediapipe/framework/getting_started/install


## Hybrid graphics

MediaPipe uses OpenGL ES to enable GPU evaluation; however this may not select
the GPU you expect on a laptop. For example, I have both an AMD Renior and an
NVIDIA RTX 2060. To ensure the NVIDIA GPU is used I need to set the [PRIME
render offload][nv_offload_url] source via:

```sh
# To check render offload setting for OpenGL (ES) on X11 (similar for Wayland?):
__NV_PRIME_RENDER_OFFLOAD=1 eglinfo | grep X11 -A 2
#   X11 platform:
#   EGL API version: 1.5
#   EGL vendor string: NVIDIA
# To run python3 with the offload setting:
__NV_PRIME_RENDER_OFFLOAD=1 python3
```

Unless you also use GLX, you do not need to set
`__GLX_VENDOR_LIBRARY_NAME=nvidia`.

NVIDIA cards also have PowerMizer which saves battery usage by scaling the clock
rates of graphics. The PowerMizer codes are enumerated in the
[`nvidia-settings` source][nv_powermizer_enum]. The default (=`0`) scales
rates by demand, while maximum performance (=`1`) locks in the maximum rates.
To set the latter:

```sh
nvidia-settings -a "[gpu:0]/GPUPowerMizerMode=1"
```

Another option is to use [gamemode][gamemode_gh] which also discusses the setup
for [hybrid GPUs][gamemode_gpu_setting].

[gamemode_gh]: https://github.com/FeralInteractive/gamemode
[gamemode_gpu_setting]: https://github.com/FeralInteractive/gamemode#note-for-hybrid-gpu-users
[nv_offload_url]: https://download.nvidia.com/XFree86/Linux-aarch64/535.54.03/README/primerenderoffload.html
[nv_powermizer_enum]: https://github.com/NVIDIA/nvidia-settings/blob/0cb3beffa0cb8a1f8cb405291b11a1e2eb7a4786/src/libXNVCtrl/NVCtrl.h#L2615C2-L2615C2


# Performance

Tested on a laptop with an AMD Ryzen 9 4800 HS, an NVIDIA GeForce RTX 2060
and:

-   UbuntuMATE 22.04 (kernel 6.5.3)
-   v4l2loopback (DKMS) 0.12.7
-   python 3.10.12
-   libturbojpeg 2.1.2 and [PyTurboJPEG][pyturbojpeg_gh] 1.7.2
-   mediapipe 0.10.3, opencv-contrib-python 4.8.0.76
-   [v4l2py][v4l2py_gh] 2.2.1

The segmentation step can be delegated to the GPU, while the decoding and blend
steps are currently CPU-only. The table below shows a comparison of the time
taken for each step when processing 1280x720 MJPG input:

|         |  CPU   | AMDGPU | NVGPU  |
|---------|:------:|:------:|:------:|
| Decode  |  6.1ms |   -    |   -    |
| Blend   |  3.1ms |   -    |   -    |
| Segment | 10.0ms | 12.4ms |  5.9ms |

The decode and blend steps imply ~10ms delay between the source camera and the
virtual camera so on laptop's like mine:

-   to synchronise with mic capture in OBS (or elsewhere), you may not even need
    to delay the audio, and;
-   a 60FPS webcam is easily handled (frames won't get dropped until
    FPS ≥100 or so).

NVGPU results were obtained with `GPUPowerMizerMode=1`.


# TODO:

-   Allow user to set source camera settings.
-   User interface (tkinter).
-   mmap output for the blend step (in preparation for OBS plugin).
-   (maybe) replace PyTurboJPEG with system libjpeg-turbo.
-   (maybe) include a yuv2rgb library to avoid hungry opencv operation.
-   (unlikely) rebuild selfie model using only tflite-runtime ops, and replace
    mediapipe with tflite-runtime to reduce footprint.
-   (unlikely) OpenCV Graph API might provide faster operations; see
    <https://github.com/opencv/opencv/blob/4.x/modules/gapi/misc/python/test/test_gapi_imgproc.py>


# Acknowledgements

Here's a short list of related work, inspirations, and alternatives.

-   [backscrub][backscrub_gh] - a C++ linux background removal tool.
-   Benjamin Elder: who presented the blog post titled
    ['Open Source Virtual Background'][elder_blog_url].
-   [Fufu Fang](mailto:fangfufu2003@gmail.com) who wrote the OG
    [Linux-Fake-Background-Webcam][linux_fake_bg_webcam_gh].
-   [John Emmons](mailto:mail@johnemmons.com) who wrote the
    [pyfakewebcam][pyfakewebcam_gh] module.
-   [OBS background removal plugin][obs_bg_removal_gh] - a cross-platform tool
    that does compositing within OBS.

[elder_blog_url]: https://elder.dev/posts/open-source-virtual-background
[pyfakewebcam_gh]: https://github.com/jremmons/pyfakewebcam

