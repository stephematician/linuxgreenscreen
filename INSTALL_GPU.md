<!-- SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
     SPDX-License-Identifier: MIT
 -->

MediaPipe with GPU support
==========================

As of August 2023, the [MediaPipe][mediapipe_gh] python package via PyPI is
_not_ built with GPU support. The following notes may help build and install the
package _with_ GPU support on Ubuntu 22.04.

Tested with commit [#3ac3b03][mp_3ac3b03] and:

-   UbuntuMATE 22.04.
-   GCC 12.3.0.
-   Packages:
    -   libopencv-dev 4.5.4.
    -   libavcodec-dev 7:4.4.2 (libavcodec 59.134.100).

[mediapipe_gh]: https://github.com/google/mediapipe
[mp_3ac3b03]: https://github.com/google/mediapipe/tree/3ac3b03ed59ceedb9b12a90cb44000b29a981b31


# Ubuntu 22.04

## Clone from GitHub

Firstly, we clone the source repository:

```sh
git clone git@github.com:google/mediapipe && cd mediapipe
```


## Compilers and build tools

The `protoc` application will be needed

```sh
sudo apt install protobuf-compiler
```

And a specific version of `bazel`, too, for which you will need to [update the
keyring and add a custom package repository][bazel_ubuntu_url]:

```sh
# if needed - install these to get + process keyring and add new package repo
# sudo apt install apt-transport-https curl gnupg
curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor >bazel-archive-keyring.gpg
sudo mv bazel-archive-keyring.gpg /usr/share/keyrings
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/bazel-archive-keyring.gpg] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
```

Now, MediaPipe 0.10.3 specifically requires 6.1.1 (or refer to `.bazelversion`
in the repository):

```sh
sudo apt update && sudo apt install bazel-6.1.1
sudo ln -s /usr/bin/bazel-6.1.1 /usr/bin/bazel
```

You'll probably want to tell `bazel` to calm down a bit with how many processes
and how much memory it eats. This can be done either via adding a
`.user.bazelrc` to the repository with the following contents (or add to a
`~/.bazelrc` to apply to `bazel` build commands on a user-level):

```rc
build --local_cpu_resources=4 --local_ram_resources=HOST_RAM*0.25
```

[bazel_ubuntu_url]: https://bazel.build/install/ubuntu#install-on-ubuntu


## Dependencies

As listed in the [instructions][mediapipe_install_url].

```sh
sudo apt install \
    libopencv-core-dev libopencv-highgui-dev libopencv-calib3d-dev \
    libopencv-features2d-dev libopencv-imgproc-dev libopencv-video-dev \
    libopencv-contrib-dev
```

[mediapipe_install_url]: https://developers.google.com/mediapipe/framework/getting_started/install>:


## Alterations to version-controlled files

We need to make changes to the version-controlled files: `setup.py`,
`third_party/BUILD` and `third_party/opencv_linux.BUILD`.

Firstly; the protobuf compiler might fail without the
`--experimental_allow_proto3_optional` argument which can be inserted in
`setup.py`:

```diff
--- a/setup.py
+++ b/setup.py
@@ -32,3 +32,3 @@ from setuptools.command import install

-__version__ = 'dev'
+__version__ = '0.10.3.post0'
 MP_DISABLE_GPU = os.environ.get('MEDIAPIPE_DISABLE_GPU') != '0'
@@ -224,2 +224,3 @@ class GeneratePyProtos(build_ext.build_ext):
           self._protoc, '-I.',
+          '--experimental_allow_proto3_optional',
           '--python_out=' + os.path.abspath(self.build_lib), source
```

Secondly, `avresample` seems deprecated, so instead we link to `swresample` on
Ubuntu/Debian (I suspect this could be omitted altogether), modify
`third_party/BUILD` as follows:

```diff
--- a/third_party/BUILD
+++ b/third_party/BUILD
@@ -226,3 +226,3 @@ cmake(
         "-lswscale",
-        "-lavresample",
+        "-lswresample",
         "-ldl",
```

Lastly, including OpenCV requires some bespoke information about the location of
the header files in `third_party/opencv_linux.BUILD` (as per the instructions):

```diff
--- a/third_party/opencv_linux.BUILD
+++ b/third_party/opencv_linux.BUILD
@@ -19,4 +19,4 @@ cc_library(
         #"include/arm-linux-gnueabihf/opencv4/opencv2/cvconfig.h",
-        #"include/x86_64-linux-gnu/opencv4/opencv2/cvconfig.h",
-        #"include/opencv4/opencv2/**/*.h*",
+        "include/x86_64-linux-gnu/opencv4/opencv2/cvconfig.h",
+        "include/opencv4/opencv2/**/*.h*",
     ]),
@@ -26,4 +26,4 @@ cc_library(
         #"include/arm-linux-gnueabihf/opencv4/",
-        #"include/x86_64-linux-gnu/opencv4/",
-        #"include/opencv4/",
+        "include/x86_64-linux-gnu/opencv4/",
+        "include/opencv4/",
     ],
```


## Build and install

We can build _with_ GPU capabilities by configuring the `MEDIAPIPE_DISABLE_GPU`
environment variable and invoking the build commands:

```sh
MEDIAPIPE_DISABLE_GPU=0 pip wheel .
pip install dist/*.whl
```


# Epilogue

`bazel` will likely create a large cache in `~/.cache/bazel` - it's best to
clean this up, e.g.

```sh
bazel clean --expunge
rm -rf $(bazel info repository_cache)
# optional - get rid of _all_ bazel cache
rm -rf ~/.cache/bazel
```

The changes made to the source can be stashed and applied whenever you want to
update MediaPipe, e.g.

```sh
git stash
git pull
git stash apply # apply changes without removing them from the stash
git stash drop  # remove the stashed changes
```


# References

-   <https://developers.google.com/mediapipe/framework/getting_started/install>:
    The official installation guide.
-   <https://github.com/dmitriykovalev/opencv-bazel>: Some information on
    building OpenCV (which MediaPipe requires) on Ubuntu.
