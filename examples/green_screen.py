# SPDX-FileCopyrightText: 2023 Stephen Wade <stephematician@gmail.com>
# SPDX-License-Identifier: MIT

from functools import partial
import signal
import sys
import cv2

from linuxgreenscreen.camera_source import CameraSource
from linuxgreenscreen.green_compositor import GreenCompositor
from linuxgreenscreen.mask_params import MaskParams
from linuxgreenscreen.virtual_sink import VirtualSink
from v4l2py.device import PixelFormat


def handle_sigquit(app_flags: dict, signal, frame):
    app_flags['exit'] = True


def main() -> int:

    video_nr = 10
    source_width, source_height = (1280, 720)
    source_fps        = 30
    source_color_code = int(PixelFormat.MJPEG).to_bytes(4, 'little').decode()

    source_camera = CameraSource(0, width=source_width, height=source_height,
                                 fps=source_fps, in_color_code=source_color_code)
    mask_params = MaskParams(period=2, n_aggregate=2, decay_rate=0.9,
                             dilate_size=0, blur_size=7, threshold=0.2)
    greenify = GreenCompositor(width=source_width, height=source_height,
                               in_color_code='BGR', out_color_code='BGR',
                               delegate_to_gpu=False, mask_params=mask_params)
    sink_camera = VirtualSink(10, width=source_width, height=source_height,
                             fps=1, in_color_code='BGR', out_color_code='BGR')

    sink_camera.play()
  # hack for now to ensure 30FPS
    source_camera._camera.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1);
    source_camera._camera.set(cv2.CAP_PROP_EXPOSURE, 350);

    grabbed = True
    n_composited = 0

    app_flags = { 'exit': False }
    signal.signal(signal.SIGINT, partial(handle_sigquit, app_flags))

    while grabbed:
        grabbed, frame = source_camera()
        if grabbed and not app_flags['exit']:
            # TODO: better maintenance of period given variable FPS with
            # auto exposure
            composited_frame = greenify(frame, j_frame=n_composited)
            n_composited += 1
            sink_camera.write_buffer(composited_frame)
            sink_camera.notify()

    decode_ms = source_camera.decode_ms / source_camera.n_decode
    segment_ms = greenify.segment_ms / greenify.n_segment
    blend_ms = greenify.blend_ms / greenify.n_blend

    print('Decode = {:.3f}ms'.format(decode_ms))
    print('Segmentation = {:.3f}ms'.format(segment_ms))
    print('Blending = {:.3f}ms'.format(blend_ms greenify.n_blend))

sink_camera.close()
source_camera.close()

